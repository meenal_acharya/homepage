+++
Title = "Statische Website Hosting"
Slug = "statische-website-hosting"
Summary = '''
Zeer snelle websites. Ondersteunt elke statische sitegenerator.

Serveer uw statische website via de Commons Host CDN voor snelheid, betrouwbaarheid en beveiliging.
'''
[[resources]]
name = "icon"
src = "gauge-full.svg"
title = "Icoon van een volle snelheidsmeter"
+++
