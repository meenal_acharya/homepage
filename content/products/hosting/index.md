+++
Title = "Static Web Hosting"
Slug = "static-web-hosting"
Summary = '''
Very fast web sites. Supports any static site generator.

Serve your static website through the Commons Host CDN for performance, reliability, and security.
'''
[[resources]]
name = "icon"
src = "gauge-full.svg"
title = "Icon showing full speedometer"
+++
