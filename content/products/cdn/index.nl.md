+++
Title = "Overal CDN"
Slug = "cdn"
Summary = '''
Veel kleine servers, overal. Beveilig en accelereer je content.

Het netwerk van fysieke edge-servers is eigendom van, en worden gehost door, bijdragers. Dit geeft lage latentie aan lokale gebruikers waar nodig.

Help het netwerk uit te breiden. De Commons Host edge-serversoftware is licht en werkt zelfs op een Raspberry Pi of virtuele server.
'''
[[resources]]
name = "icon"
src = "earth.svg"
title = "Icoon van een wereldbol"
+++
