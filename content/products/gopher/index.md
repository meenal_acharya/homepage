+++
Title = "Gopher"
Slug = "gopher"
Summary = '''
Rediscover the early-days Internet of the text based Gopher protocol.

Build and host a Gopherhole on Commons Host. Browse the Gopherverse via the Commons Host Gopher over HTTP proxy service.
'''
[[resources]]
name = "icon"
src = "noun_Rodent_1415987.svg"
title = "Icon showing a gopher animal"
+++
