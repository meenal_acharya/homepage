+++
Title = "DNS over HTTPS"
Description = "Resist censorship. Protect your privacy. Accelerate your Internet."
Slug = "doh"
Layout = "doh"
Summary = '''
Privacy, security, performance. No more Big Brother. Protect yourself with Commons Host DNS over HTTPS to encrypt your domain name lookups.
'''
[[resources]]
name = "icon"
src = "shield-lock.svg"
title = "Icon showing a privacy shield"
+++

## How It Works

An encrypted, long-lived HTTPS connection to a DNS resolver shields your DNS traffic from spying or tampering.

When your browser looks up a domain name, the request is sent out immediately over the already established HTTPS connection.

Unlimited DNS queries are sent and received in parallel, as multiplexed streams. As fast as possible.

## Usage

Any custom domain or subdomain hosted on the Commons Host CDN automatically supports DoH. The DoH provider URL used by DoH clients can be:

- The default domain of `https://commons.host`, or
- Your subdomain like `https://example.commons.host`, or
- Your custom domain like `https://example.net`.

### Dohnut: DoH Proxy

Use [Dohnut](https://help.commons.host/dohnut/), the Commons Host DoH proxy, to use DoH across all your software and devices.

Dohnut supports load balancing, query spoofing, connection sharding, and other advanced features to increase your DNS performance and privacy.

Dohnut can run on your personal device, or on a separate machine like a Raspberry Pi.

Enable DoH across all devices on your local network using the DHCP settings of your local network router. Configure your router's DNS resolver to use the IP address of the DoH proxy.

Documentation: https://help.commons.host/dohnut/

### Firefox

The Mozilla Firefox browser natively supports DoH.

{{< figure src="./firefox-settings-doh.png" title="Screenshot of Firefox DoH preferences" width="671" height="116" >}}

**Preferences** > **Network Settings** > **Enable DNS over HTTPS**

- **Use Provider**: *Custom*
- **Custom**: `https://commons.host`

Documentation: [Firefox DNS-over-HTTPS - Switching providers](https://support.mozilla.org/en-US/kb/firefox-dns-over-https#w_switching-providers)

### Chrome

The Google Chrome browser natively supports DoH.

{{< figure src="./chrome-settings-doh.png" title="Screenshot of Chrome DoH preferences" width="507" height="208" >}}

**Preferences** > **Privacy and Security** > **More** > **Use secure DNS**

- **With**: *Custom*
- **Enter custom provider**: `https://commons.host`

Documentation: [Chromium Blog - A safer and more private browsing experience with Secure DNS](https://blog.chromium.org/2020/05/a-safer-and-more-private-browsing-DoH.html)

## Domain Name Filtering

*Coming soon*

Protect your device by preventing connections to undesired servers. Choose from community curated blacklists or configure your own custom domain name filters.

Block advertisers, trackers, spyware, malware, and other bad actors.

Create a family-friendly or professional environment on the Internet by blocking undesired content categories.

## Hybrid Local/Cloud Deployment

*Coming soon*

For ultimate performance and privacy, run a local Commons Host Edge server on your own hardware, within your private network. On the local network, any DoH clients or proxy connect to your local private Edge server which is a fully independent DNS resolver. When you leave the network to go outside, they seamlessly fall back to the Commons Host CDN.

## Speed Test

Measure and compare the latency of DoH/DNS resolvers from your own device.

https://help.commons.host/bulldohzer/cli/

Tip: Dohnut can monitor multiple DoH providers and automatically use the lowest latency resolver from your location using the [`--load-balance performance` option](https://help.commons.host/dohnut/cli/#--load-balance-performance).

{{< products/doh-benchmark >}}
